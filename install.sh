#!/bin/sh

 # Récupérer les dossiers avant toute opération
 ma_position=`pwd`
 mon_home=$HOME

 #Sauvegarde des claviers et installation
 echo "Dossier local :"
 echo $ma_position
 cd /usr/share/X11/xkb/symbols
 echo "Changement d'emplacement vers :"
 pwd
 echo "Sauvegarde du clavier grec"
 sudo mv gr gr.old
 echo "Sauvegarde du clavier français"
 sudo mv fr fr.old
 echo "Installation du clavier grec"
 sudo cp "$ma_position/gr" $PWD/
 echo "Installation du clavier français"
 sudo cp "$ma_position/fr" $PWD/
 cd /usr/share/X11/xkb/rules
 echo "Changement d'emplacement vers :"
 pwd
 echo "Sauvegarde du fichier evdev.xml"
 sudo mv evdev.xml evdev.xml.old
 echo "Installation du fichier evdev.xml"
 sudo cp "$ma_position/evdev.xml" $PWD/
 cd "$mon_home"
 echo "Changement d'emplacement vers :"
 pwd
 echo "Sauvegarde de .XCompose"
 sudo mv .XCompose .XCompose.old
 echo "Installation du fichier .XCompose"
 sudo cp "$ma_position/.XCompose_manjaro" $PWD/.XCompose
