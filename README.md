Clavier grec moderne réadapté pour le grec ancien

# Origine du clavier

## Etat des lieux

 Les héllenistes trouveront sur internet trois types de claviers grecs actuellement:
 
 1. Clavier "greek polytonic" de la distribution Ubuntu officielle
 2. Clavier "grc (Grec Ancien)" de Xexanoth, que l'on trouve sur la [documentation Ubuntu france](https://doc.ubuntu-fr.org/logiciels_pour_le_lycee#grec_ancien)
 3. Clavier "Hypathie" que l'on trouve sur le [forum Debian](https://debian-facile.org/doc:environnements:x11:disposition-grecque-polytonique#fnt__1)

Les inconvénients de chacun, sont selon moi les suivants (respectivement) :

1. Qwerty!
2. Clavier trop sommaire
3. L'accentuation n'est pas à mon avis à l'endroit le plus intuitif

Il me semble important, pour qu'un clavier grec soit utilisable, qu'il colle aussi près que possible au clavier français. D'où ces quelques principes qui dirigent la composition de celui-ci:

## Principes

- La localisation des caractères est maintenue autant que possible proche de celle d'un clavier français azerty, en particulier pour les caractères spéciaux de la rangée des chiffres (parenthèses, guillemets, etc)
- La ponctuation est transposée selon l'usage (ainsi, je remplace le point d'interrogation par le point-virgule, etc)
- Les caractères dérivés d'une autre lettre (ϐ, ς, iota souscrit) sont positionnés sur leur équivalent, au niveau 3 (c'est-à-dire qu'on y accède avec AltGr)
- Les caractères spéciaux (par exemple ϻ, ϝ, ϡ, ϑ, ϖ...) les plus fréquents sont référencés autant que possible sur les niveaux 3 et 4. (peuvent s'atteindre avec altgr et altgr+maj)
- Le clavier de Denis Liégeois pour Windows ne constitue pas ma référence pour la localisation de l'accentuation et des lettres.
- Il n'y a pas de convention pour localiser sur le clavier les caractères ξ, ψ, θ. Mes choix sont liés à ma pratique du clavier grec (respectivement: J, C, Y).
- Le clavier est proche autant que possible de mon clavier "Français - Linguistique", afin de ne pas s'emmêler les pinceaux pour l'accentuation.
- L'accentuation est située essentiellement sur les touches situées entre les caractères alphanumériques et les touches entrée / maj / retour.

Le principe de ce clavier est simple: si, à l'usage, une de ces conventions vous paraît absurde, il est parfaitement possible d'aller changer les positions des caractères (il suffit de modifier le fichier `gr`, c'est très intuitif, voir plus bas).
N'hésitez pas à me signaler de telles modifications : si la solution est pertinente, je peux les intégrer dans le clavier.

## Limites techniques

La combinaison de diacritiques (c'est-à-dire les enchaînement de caractères non autonomes, comme les accents, et les esprits) n'est pas automatique : il faut définir les combinaisons possibles. Cette définition est le plus souvent déjà effectuée dans les fichiers de l'ordinateur, mais ces caractères accentués ne sont pas toujours activés par défaut.
La combinaison de diacritiques sous Ubuntu est conditionnée par le "serveur" (c'est-à-dire le système) utilisé pour le clavier :
	- Ibus (par exemple dans Unity, Gnome) : a priori, pas de problème (sinon, suivre instructions infra)
	- uim : TOUTES NE MARCHENT PAS! Par conséquence, il est conseillé de copier-coller le fichier .XCompose ci-joint dans le répertoire personnel. Permet de disposer d'un certain nombre d'autres combinaisons (d'autres peuvent être paramétrées en complétant le fichier .XCompose).
    - KDE est plus spécifiquement touché : les diacritiques y posent problème régulièrement.

# Quelques caractères utiles

## Grec classique

- θ: Y (le U est dévolu au upsilon)
- ψ: C
- ξ: J
- ς: Q ou AltGr + S
- ϐ: AltGr + B
- ͺ(iota souscrit) : AltGr + I

## Grec archaïque, histoire de l'alphabet, phénicien...

- ϝ, ϟ, ϻ... sont répertoriés dans le tableau ci-dessous

## Caractères de la linguistique grecque

- y (yod) : AltGr+y
- h (aspiration) : AltGr+h
- ϝ (digamma) : AltGr+G (G comme gamma)
- \* (astérisque) : AltGr + \* (au niveau 1 la touche donne des accents)

## Ponctuation transposée par rapport aux normes typographiques du français

- ? donne ; (signe d'interrogation en grec ancien)
- ; donne · (point médian remplace le point virgule)
- , reste , (identique)
- . reste . (identique)
- : reste : (identique)

J'ai considéré qu'en grec ancien, on n'avait besoin ni de ù, ni de %, $, £, ¤, etc. Si toutefois un symbole Unicode apparaissait manquant et pourtant utile, ne pas hésiter à me contacter pour que je l'ajoute !

# Diacritiques

## Localisation

Ils se concentrent sur quatre touches spécifiques au clavier français, et donc réaffectées: 

1. "^ / ¨" : circonflexe / tréma
2. "$ / £" : longue / brève (avec Maj) / tiret long / opérateur d'union (permettent éventuellement de symboliser une scansion)
2. "ù / %" : esprit doux / accent aigu (avec Maj) - accent aigu se trouve aussi en "2" au premier niveau
3. "\* / µ" : esprit rude / accent grave (avec Maj) - accent grave se trouve aussi en "7" au premier niveau

## Exemples de la composition de diacritiques

- esprits rudes et doux: [<ù>/<\*>] + voyelle
- accents aigus et graves: [<2>/<7>) + voyelle (position de é /è sur un clavier français) ou MAJ + [<ù>/<*>] + voyelle (donc au dessus des esprits)
- accents circonflexes et trémas: comme d'ordinaire
- voyelle longue: <$> + voyelle (macron)
- voyelle brève: MAJ + <$> + voyelle (breve)
- iota souscrit: AltGr + <i>
- La composition de diacritiquées marche: par exemple pour faire ᾔ il suffit de faire:
	- 2 ou Μaj+ù  }
	- ù	     } DANS l'ORDRE QUE L'ON SOUHAITE SAUF EXCEPTION (dépend du serveur, cf supra)
	- altgr+i     }
	- h (=η)		->MAIS /!\ LA VOYELLE A LA FIN /!\
	
Si cela ne marche pas, il suffit d'ajouter le fichier .XCompose dans le répertoire personnel (explications infra)

# Fichiers de ce répertoire

Ce répertoire contient quatre fichiers:
- Le fichier `gr` contient la disposition de clavier proprement dite. Il s'agit d'une version complétée de la disposition "Grec moderne" de la dernière version d'Ubuntu (la 20.04). A priori, elle marche sur toutes les distros récentes.
- Le fichier `fr` est un bonus pour linguistes: il permet d'inclure des caractères diacritiques propres à la phonétique historique (ogonek, etc) et à la linguistique indo-européenne ou finno-ougrienne.
- Le fichier `evdev.xml` sert à Ubuntu à localiser les claviers disponibles. Il s'agit d'une version complétée du fichier `evdev.xml` de la dernière version d'Ubuntu (la 20.04). A priori, elle marche sur toutes les distros récentes.
- Le fichier `.XCompose` sert à compléter les compositions de diacritiques non fonctionnelles. Voir infra.

# Installation

En ligne de commande seulement (ce n'est pas très difficile :-), il suffit de copier-coller les instructions).

## Explications

1. Le terminal s'ouvre avec Ctrm+Alt+T ou en tapant "Terminal" dans la barre de recherche
2. "sudo" signifie "en mode superutilisateur", donc le terminal va vous demander le mot de passe. On ne peut pas modifier les configurations clavier sans passer par là.
2. "cp" signifie "copy-paste", c'est à dire "copier-coller". Il est suivi de la localisation du fichier originel, et de la destination où on veut le copier.

## Instructions

1. Le fichier "gr" : Copier-coller l'ancien fichier pour en faire une sauvegarde

`sudo cp /usr/share/X11/xkb/symbols/gr /usr/share/X11/xkb/symbols/gr.old`

2. Le fichier "gr" : Le remplacer par le nouveau fichier

`sudo cp /<emplacement du fichier>/gr /usr/share/X11/xkb/symbols/gr`

3. Le fichier "evdev.xml" : Copier-coller l'ancien fichier pour en faire une sauvegarde

`sudo cp /usr/share/X11/xkb/rules/evdev.xml /usr/share/X11/xkb/rules/evdev.old.xml`

4. Le fichier "evdev.xml" : Le remplacer par le nouveau fichier

`sudo cp /<emplacement du fichier>/evdev.xml /usr/share/X11/xkb/rules/evdev.xml`

 Puis, sélectionner manuellement le clavier dans les paramètres. Il devrait apparaître sous le nom "Ancient Greek - Azerty"
 
## Cas particulier : le fichier `.XCompose`

Le fichier .XCompose définit pour l'ordinateur la liste des traitements des diacritiques. Un certain nombre de traitements sont renseignés par défaut. Ainsi, si l'on tape "e" + "accant aigu", toutt bonne distribution ubuntu vous donne "é", même si le fichier .XCompose est vide.
C'est que ce fichier référence les paramètres personnels de l'utilisateur (ie, ceux que vous voulez ajouter). Il se place normalement dans le dossier utilisateur (`/home/<utilisateur>`), à l'instar de tous les fichiers de configuration.
Sa syntaxe comporte:
1. une ou des lignes `include blablabla`: spécifie les fichiers préparamétrés à inclure dans vos paramètres. Par défaut, votre ordinateur n'utilise pas les combinaisons de l'albanais, mais si vous en avez besoin, il suffit de l'indiquer ici.
2. Des lignes composées ainsi <Réf du diacritique 1> <Réf du diacritique 2> <Réf du caractère > : "caractère unicode résultant" , qui indiquent vos souhaits de diacritiques.

Mon fichier .XCompose est très perfectible. S'il vous manque des compositions, n'hésitez pas à les ajouter vous-mêmes!
 
# A propos

Ces claviers ont été créés au fur et à mesure pour mon usage. Ils ne sont pas parfaits, ne reflètent que ma pratique du grec. Certains préfèreront avoir les accents ailleurs, etc.
Sur un git, les fichiers peuvent être modifiés par tout un chacun, car tout le monde peut créer une nouvelle branche et améliorer le code de son côté.
Vous pouvez aussi, si vous avez besoin de deux ou trois caractères spécifiques et qui n'y sont pas, me contacter pour que je les ajoute: je n'en ai peut-être jamais eu besoin, tout simplement.

Pour tout problème, ouvrir un ticket, dans la section "issue". Si vous ne vous y connaissez pas bien et avez juste besoin d'aide pour installer le clavier, n'hésitez pas, je vous aiderai avec plaisir.
